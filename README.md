<<<<<<< HEAD
<h1>morn-framework:</h1>
<h3>1.morn-webmvc</h3>
    1.1 实现了mvc基本功能
    	在web.xml中配置：
    	<filter>
			<filter-name>mornMvc</filter-name>
			<filter-class>com.inmorn.webmvc.filter.MornFilter</filter-class>
			<init-param>
				<param-name>scanPackage</param-name>
				<param-value>xx.xxx.package</param-value>
			</init-param>
		</filter>
		<filter-mapping>
			<filter-name>mornMvc</filter-name>
			<url-pattern>/*</url-pattern>
		</filter-mapping>
	
	1.2 基本注解
	   Action类统一使用@Action
	   Service统一使用@Service
	   Dao类可使用@MyBatisMapper
	       组件类可使用@Component
		
    1.3 实现了拦截器功能
                    使用拦截器需实现com.inmorn.webmvc.interceptor.ActionInterceptor接口,
                   以及在实现类中加注解@Interceptor(order是拦截器执行顺序,数字越小越先执行;path是需要拦截前端url路径:/**或/user/**)
                   例如：
           @Interceptor(order=0,path="/**")
		   public class LogInterceptor implements ActionInterceptor{
		
			  public boolean beforeAction(HttpServletRequest request,
					HttpServletResponse response, Object action) {
				 return true;
			  }
		
			  public void afterAction(HttpServletRequest request,
					HttpServletResponse response, Object action, Object result) {
			  }
		
			  public void completionAction(HttpServletRequest request,
					HttpServletResponse response, Object action, Exception e) {
			  }
		 }
                    
   
<h3>2.morn-context</h3>
	2.1 统一管理上下文Bean,可扩展Bean,自定义注解解释器
	2.2 加入@Bean注解以代替繁琐的配置文件
	2.3 扩展接口：BeanPostProcessor.java,InitializingBean.java,RealityBean.java
	    FactoryBeanAware.java,DisposableBean.java,RegisterAnnotationResole.java


<h3>3.morn-orm-mybatis</h3>
	3.1 整合MyBatis,可以通过MyBatis注解(@Select...)以及xxx-mapper.xml对数据库操作
	3.2 加入MyBatisMapper新注解,对Dao接口注解并标识SqlSessionFactoryBeanName,方便多数据源使用
	使用时需配置:
	<!-- 数据源配置 -->
	@Bean(classes="org.apache.commons.dbcp.BasicDataSource",propertys={
			@Property(name="driverClassName",value="${oracle.driver}"),
			@Property(name="url",value="${oracle.url}"),
			@Property(name="username",value="${oracle.username}"),
			@Property(name="password",value="${oracle.password}")
	})
	Object core_oracle_ds_rw;
	
	<!-- sqlSessionFactory配置 --> 
	@Bean(classes="com.inmorn.orm.mybatis.SqlSessionFactoryBean",
		  propertys={@Property(name="dataSource",ref="core_oracle_ds_rw"),
		  			 @Property(name="mapperLocations",value="classpath:com/morn/testweb/*/mapping/*.xml")})
	Object sqlSessionFactory;
	
	<!-- 注册MyBatisMapper注解解释器 -->
	@Bean(classes="com.inmorn.orm.mybatis.MapperScannerConfigurer")
	Object mapperScannerConfigurer;
   
    <!-- 数据访问层接口 -->
    @MyBatisMapper(sqlSessionFactoryBeanName="sqlSessionFactory")
	public interface UserDao {

		@Select("select e.code userName,e.name address from ytmdm.t_mdm_employee e where e.code = #{id} ")
		public User findById(String id);
	
	}
 
 
 
 
 <br/><br/><br/><br/><br/>
 
[愿景]：
     1.希望热爱JAVA的朋友能为开源做一些贡献,提出自己的想法,解决方案
     2.也希望有人或对此框架有爱好的提出宝贵的意见,共同参与此框架的开发与设计
     3.同时也希望能够提升自己的专业能力
                
			引用一句话：任重而道远!
					         热爱生活,共同努力
							Tihnks.
