package com.morn.testweb.beans;

import com.inmorn.context.annotation.Component;
import com.inmorn.context.beans.exception.BeanInitializeException;
import com.inmorn.context.beans.extend.BeanPostProcessor;

@Component
public class SayBeanHelloBeanPostProcessor implements BeanPostProcessor{

	public int getOrder() {
		return 2;
	}

	public Object postProcessBeforeInitialization(Object bean, String beanName)
			throws BeanInitializeException {
		System.out.println(" SayBean before beanName:" + beanName);
		return bean;
	}

	public Object postProcessAfterInitialization(Object bean, String beanName)
			throws BeanInitializeException {
		System.out.println(" SayBean after beanName:" + beanName);
		return bean;
	}

}
