package com.morn.testweb.beans;

import com.inmorn.context.annotation.Component;
import com.inmorn.context.beans.extend.FactoryBeanAware;
import com.inmorn.context.beans.extend.RegisterAnnotationResolve;
import com.inmorn.context.beans.factory.BeanDefinition;
import com.inmorn.context.beans.factory.FactoryBean;
import com.inmorn.context.beans.factory.PropertyDefinition;

@Component
public class IBatisDaoAnnotationResole implements RegisterAnnotationResolve,FactoryBeanAware{

	public Class<?> annotationClass = IBatisDao.class;

	public FactoryBean factoryBean;
	
	public Class<?> getAnnotationClass() {
		return annotationClass;
	}

	public BeanDefinition process(BeanDefinition beanDefinition) {
		System.out.println(beanDefinition.getBeanName() + "\t factoryBean:" + factoryBean);
		beanDefinition.setBeanFlag(FactoryBean.CONTEXT_BEAN_FLAG_BEANS);
		IBatisDao iBatisDao = beanDefinition.getBeanClass().getAnnotation(IBatisDao.class);
		
		PropertyDefinition propertyDefinition = new PropertyDefinition(null, null, iBatisDao.value());
		beanDefinition.getPropertys().put("dataSource", propertyDefinition);
		return beanDefinition;
	}

	public void setFactoryBean(FactoryBean factoryBean) {
		this.factoryBean = factoryBean;
	}

}
