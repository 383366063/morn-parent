/**
 * Copyright 2016- the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.morn.testweb.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.inmorn.orm.mybatis.annotation.MyBatisMapper;
import com.morn.testweb.domain.User;

@MyBatisMapper(sqlSessionFactoryBeanName="mysql_sqlSessionFactory")
public interface StationUserInfoDao {

	@Select("select e.code userName,e.name address from t_mdm_district e where e.code = #{id} ")
	public User findById(String id);
	
	@Insert("insert into t_mmm(id,name) values(#{user.userName},#{user.address}) ")
	public int insertInfo(@Param("user")User user);
	
	public List<User> selectAll(Map<String, Object> param);
	
}
